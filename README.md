Pasteur
==============================

Análisis de publicaciones, grupos, páginas de Facebook para caracterizar mensajes anti-vacunación que buscan ralentizar o limitar el avance de la campaña de vacunación contra el COVID19 en México.

Partners: SocialTIC + Deutsche Welle

Justificación
-------------

Deutsche Welle y SocialTIC están realizando un mapeo de grupos e iniciativas anti-vaxxers, y sobre todo su nivel de sofisticación del discurso o técnicas de persuasión en LATAM. En México la campaña de vacunación contra el COVID se está viendo afectada por varios factores, entre ellos:

1.  Poca efectividad y planeación de las autoridades de salud

2.  Comunicaciones oficiales poco veraces o verificables

3.  Zonas de difícil acceso en sureste del territorio nacional

Sin embargo, hay una parte de la población que decide no vacunarse debido a la posible efectividad de campañas anti-vacunas que circulan en redes sociales.

Es necesario caracterizar tanto los mensajes, aquellos que las crean, y la población dentro de la red social que es susceptible a ser motivada a la inacción debido a estos mensajes.

Planteamiento de la solución
----------------------------

Será necesario desarrollar los sig. modelos de análisis de texto:

1.  TF/IDF lematizado para densidades de palabras

2.  Topic modelling (LDA u otros) para agrupar términos y mensajes en tópicos

Esto nos permitirá mayor rigor que solamente una contextualización y podremos soportar o rechazar mejor la hipótesis del efecto que estos mensajes tienen en el número de personas vacunadas a la fecha.

El producto final será un microwebsite contando la historia de la investigación e ilustrando los resultados.

Actividades
-----------

-   Definición de palabras clave para búsquedas y descargas de CrowdTangle

-   Descarga de posts en páginas, grupos públicos y perfiles

-   Categorización de tipos de páginas, grupos y perfiles y sus combinaciones 

-   Análisis de texto de mensajes en publicaciones

-   Análisis de contexto de publicaciones y comparación con reacciones de Facebook

-   Visualización de características resultantes del análisis de texto, tópicos y tono

-   Desarrollo de microwebsite con storytelling tanto de la investigación como de sus conclusiones

Definiciones y Criterios
------------------------

### Cuál es la hipótesis?

Hay una fracción de población no vacunada que está en ese status debido a mensajes y desinformación esparcida por Facebook. Si esto es cierto, es necesario caracterizarla.

### Hay algún ejemplo de estos artefactos de desinformación?

![](https://lh5.googleusercontent.com/jVtaRZNh-XQWMIoLT1Q_bNkxrwybAeWY9Sm7iakWSagBN4L02_cOn2K_D88PL6bnyBR_tSEWAIr1mtFCZXt8llIDkbdU-s3HkLuQ8siKi8FUUyjjLVQjTVyATOzZ_asXJyMeLc5h=s0)

Recursos
--------

1.  [Falso que el gobierno suspendió pagos de servicios por contingencia (animalpolitico.com)](https://www.animalpolitico.com/elsabueso/suspension-pagos-servicios-covid-19-falso/)

2.  [Fake News -- Covid 19 -- Ceaip Sinaloa](https://covid.ceaipsinaloa.org.mx/category/dai-vs-fake-news/)

Equipo
------

-   Quién lleva el kanban board (PM)?

-   Quién le dará principalmente a la tecla (Data Scientists)?

-   Quienes le darán a la tecla part-time (Data Scientists)?

-   Quién revisa que la tecla esté bien (Mentor Técnico)?

-   Quién revisa que la tecla se apegue al problem domain (Business Specialist)?

Project Organization
------------

**Es crucial seguir esta estructura al pie de la letra** para facilitar los esfuerzos de project management.


    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
