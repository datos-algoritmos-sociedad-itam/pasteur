from huggingface/transformers-pytorch-gpu
ADD requirements.txt /workdir/
RUN pip install -r /workdir/requirements.txt
RUN useradd --create-home --shell /bin/bash pasteur
USER pasteur
WORKDIR /home/pasteur
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
