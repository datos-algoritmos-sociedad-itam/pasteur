from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Análisis de publicaciones, grupos, páginas de Facebook para caracterizar mensajes anti-vacunación que buscan ralentizar o limitar el avance de la campaña de vacunación contra el COVID19 en México.',
    author='Centro ITAM para Datos + Algoritmos + Sociedad',
    license='MIT',
)
